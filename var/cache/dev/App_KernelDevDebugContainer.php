<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerX8htkep\App_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerX8htkep/App_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerX8htkep.legacy');

    return;
}

if (!\class_exists(App_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerX8htkep\App_KernelDevDebugContainer::class, App_KernelDevDebugContainer::class, false);
}

return new \ContainerX8htkep\App_KernelDevDebugContainer([
    'container.build_hash' => 'X8htkep',
    'container.build_id' => '4c52bf31',
    'container.build_time' => 1632361745,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerX8htkep');
